<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new \App\Category);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('slug', 'slug')->sortable();
        $grid->column('name', 'name')->sortable();
        $grid->column('description', 'description')->sortable();
        $grid->column('enabled', 'enabled')->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(\App\Category::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('slug', 'slug');
        $show->field('name', 'name');
        $show->field('description', 'description');
        $show->field('enabled', 'enabled');
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new \App\Category);

        $form->display('id', __('ID'));
        $form->text('slug', 'slug');
        $form->text('name', 'name');
        $form->text('description', 'description');
        $form->text('enabled', 'enabled');
        $form->display('created_at', __('Created at'));
        $form->display('updated_at', __('Updated at'));

        return $form;
    }
}
