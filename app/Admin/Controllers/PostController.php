<?php

namespace App\Admin\Controllers;

use App\Category;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Post;

class PostController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Post);

        $grid->model()->withCount('categories');
        $grid->column('id', __('ID'))->sortable();
        $grid->column('slug', 'slug')->sortable();
        $grid->column('name', 'name')->sortable();
        $grid->column('description', 'description')->sortable();
        $grid->column('enabled', 'enabled')->sortable();
        $grid->column('categories_count');
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Post::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('slug', 'slug');
        $show->field('name', 'name');
        $show->field('description', 'description');
        $show->field('enabled', 'enabled');
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Post);

        $form->display('id', __('ID'));
        $form->text('slug', 'slug');
        $form->hidden('name', 'name')->default('[]');
        $form->hidden('description', 'description')->default('[]');
        // name => {'zhtw' => 't1', 'en' => 'en1'}

        $form->switch('enabled', 'enabled');
        $form->multipleSelect('categories')->options(Category::all()->pluck('name', 'id'));
        $form->display('created_at', __('Created at'));
        $form->display('updated_at', __('Updated at'));

        foreach (config('app.locales') as $code => $label) {
            $form->divider($label);
            $cLocale = \Str::camel($code);  // zh-tw => zhtw
            $form->hidden("{$cLocale}.locale")->default($code);
            $form->text("{$cLocale}.name", __('name'));
            $form->textarea("{$cLocale}.description", __('description'));
            $form->switch("{$cLocale}.active", __('active'));
        }

        return $form;
    }
}
