<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasEnabled, HasTransaction;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'description', 'enabled'
    ];

    protected $appends = [
        'virtual_name'
    ];

    protected $casts = [
        'name' => 'json',
        'description' => 'array',
    ];

    public function getVirtualNameAttribute()
    {
        return 'aaa';
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // public function translations()
    // {
    //     return $this->hasMany(static::getTranslationClass());
    // }

    // public function en()
    // {
    //     return $this->hasOne(PostTranslation::class)->where('local', 'en');
    // }

    // public function zhtw()
    // {
    //     return $this->hasOne(PostTranslation::class)->where('local', 'zh-tw');
    // }

    // public function es()
    // {
    //     return $this->hasOne(PostTranslation::class)->where('local', 'es');
    // }
}
