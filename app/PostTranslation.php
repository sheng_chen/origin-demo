<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'locale', 'active', 'name', 'description'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    
    public static function boot()
    {
        parent::boot();

        self::saved(function ($model) {
            foreach (['name', 'description'] as $attr) {
                $value = $model->post->$attr;
                # Arr::set() 第一個參數不能使用$model->attr
                $model->post->$attr = \Arr::set($value, $model->locale, $model->name);
                $model->post->save();
            }
            // 棄用
            // $post = $model->post()->first();
            // $name = $post->name;
            // $post->name = \Arr::set($name, $model->locale, $model->name);
            // $post->save();
        });
    }
}
