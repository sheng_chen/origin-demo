<?php
namespace App;

trait HasEnabled
{
    protected static function bootHasEnabled()
    {
        static::addGlobalScope('enabled', function ($query) {
            if (!request()->is(config('admin.route.prefix') . '/*')) {
                $query->where('enabled', 1);
            }
            return $query;
        });
    }

    protected function initializeEnabled()
    {
        return $this->append('virtual_name');
    }
}
