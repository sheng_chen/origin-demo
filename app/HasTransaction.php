<?php
namespace App;

trait HasTransaction
{
    protected static function getTranslationClass()
    {
        return static::class . 'Translation';
    }

    public function translations()
    {
        return $this->hasMany(static::getTranslationClass());
    }

    public function en()
    {
        return $this->hasOne(static::getTranslationClass())->where('locale', __FUNCTION__);
    }

    public function zhtw()
    {
        return $this->hasOne(static::getTranslationClass())->where('locale', 'zh-tw');
    }

    public function es()
    {
        return $this->hasOne(static::getTranslationClass())->where('locale', __FUNCTION__);
    }
}
